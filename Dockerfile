FROM golang as build
RUN apt-get update && apt-get install -y apt-transport-https
RUN apt-key adv --keyserver pgp.mit.edu --recv-keys 9FD3B784BC1C6FC31A8A0A1C1655A0AB68576280
RUN echo 'deb https://deb.nodesource.com/node_9.x jessie main' > /etc/apt/sources.list.d/nodesource.list
RUN apt-get update && apt-get install -y nodejs
RUN go get github.com/go-swagger/go-swagger/cmd/swagger
RUN go get github.com/golang/dep/cmd/dep
COPY . /go/src/git.callpipe.com/finn/piston-go
WORKDIR /go/src/git.callpipe.com/finn/piston-go
RUN swagger generate server -P piston.BrowserSubscription -f ./api-spec.yml
RUN dep ensure
RUN go build ./cmd/piston-server
RUN npm install
RUN npx webpack

FROM debian:stable
RUN useradd piston
EXPOSE 3000
RUN apt-get update && apt-get install -y ca-certificates && apt-get clean
COPY --from=build /go/src/git.callpipe.com/finn/piston-go/piston-server /usr/local/bin/piston-server
COPY --from=build /go/src/git.callpipe.com/finn/piston-go/static /var/lib/piston/static
WORKDIR /var/lib/piston
USER piston
CMD /usr/local/bin/piston-server --host=0.0.0.0 --port 3000
