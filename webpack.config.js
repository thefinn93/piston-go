const MiniCssExtractPlugin = require("mini-css-extract-plugin");


module.exports = {
  entry: "./static/js/piston.js",
  output: {
    path: __dirname + '/static/dist',
    filename: 'piston.bundle.js',
    publicPath: "/dist"
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader"
        ]
      }
    ]
  }
};
