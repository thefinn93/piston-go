self.addEventListener('push', function(event) {
  var payload = event.data.json();
  payload.options.data = {};
  payload.options.data.callbackURL = payload.callbackURL;
  payload.options.data.browserURL = payload.browserURL;
  if(payload.options.actions == null) {
    payload.options.actions = [];
    console.log(payload.options);
  }
  console.log(payload);
  event.waitUntil(
    self.registration.showNotification(payload.title, payload.options)
  );
});

self.addEventListener('notificationclick', function(event) {
  var notification = event.notification;
  console.log("Got notification click event!", event);

  event.waitUntil(new Promise((resolve, reject) => {
    if(notification.data.browserURL) {
      console.log("BrowserURL set to", notification.data.browserURL);
      if(clients.openWindow) {
        console.log("Opening BrowserURL in clients", clients);
        resolve(clients.openWindow(notification.data.browserURL));
      }
    } else {
      console.log("No BrowserURL specificed");
      resolve()
    }
  }).then(() => {
    console.log("CallbackURL step");
    if(notification.data.callbackURL) {
      console.log("CallbackURL set to", notification.data.callbackURL);
      return fetch(notification.data.callbackURL, {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: {
          action: notification.action,
          subscriber: notification.data.subscription,
          id: notification.data.id
        }
      });
    } else {
      console.log("No callback URL set");
    }
  }).then(() => {
    console.log("Closing notification!");
    notification.close();
  }).catch((e) => {
    console.log(e);
  }));

});
