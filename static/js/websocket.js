const ReconnectingWebSocket = require('reconnecting-websocket');

var ws = new Promise((resolve, reject) => {
  var socket = new ReconnectingWebSocket("wss://" + window.location.hostname + "/ws");
  socket.onmessage = onMessage;
  socket.onopen = (e) => {resolve(socket);};
  socket.onerror = (err) => {reject(err);};
  socket.onclose = onClose;
});

function send(message) {
  return ws.then((socket) => {
    console.log("[Websocket] Us:", message);
    socket.send(JSON.stringify(message));
  });
}

function onMessage(e) {
  var message = JSON.parse(e.data);
  if(message.Channel) {
    console.log("Channel updated", message.Channel);
    updateChannel(message.Channel);
  } else {
    console.log("Unknown websocket message", message);
  }
}

function onClose(e) {
  console.log("Websocket closed", e);
}

module.exports = {
  send: send
}
