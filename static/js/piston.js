import tippy from '../../node_modules/tippy.js/dist/tippy.standalone.js'
import QRCode from 'qrcode'
import styles from '../css/piston.css'
import ws from './websocket.js';


function init() {
  document.querySelector('.create-channel').addEventListener('submit', createChannel);
  document.querySelector('.toggle-subscription').addEventListener('click', toggleSubscription);
  window.addEventListener('hashchange', onHashChange);
  document.querySelector('.delete-channel-btn').addEventListener('click', deleteChannel);
  document.querySelector('.send-notification-btn').addEventListener('click', testChannel);
  window.configready = f('/api/v1/config').then((c) => {window.config = c; return c})
  navigator.serviceWorker.register('/service-worker.js');
  onHashChange();
  tippy('.tippy');
}

function setText(querySelector, text) {
  console.debug("setText(", querySelector, text, ")");
  document.querySelectorAll(querySelector).forEach((elm) => {
    elm.innerText = text;
  });
}

function renderChannel(channelid) {
  ws.send({action: "subscribe", channel: channelid});
  var subscribebtn = document.querySelector('.toggle-subscription');
  subscribebtn.dataset.channel = channelid;
  renderQRCode();
  var notificationsApproved = detectNotificationsApproved(); // true if the user has allowed us to send notifications
  console.debug("Notifications Approved?", notificationsApproved);
  if(notificationsApproved) {
    navigator.serviceWorker.ready.then((r) => {
      console.debug("ServiceWorker Ready", r);
      return r.pushManager.getSubscription();
    }).then((s) => {
      console.debug("Subscription", s);
      window.subscription = s;
      return getChannel(channelid, s); // Gets info about the channel, including the current user's subscription status
    }).then(updateChannel)
    .then((channel) => {
      console.log("Channel token:", channel.token);
      if(channel.token) {
        document.querySelectorAll('.channel-admin.hidden').forEach((elm) => {
          elm.classList.remove('hidden');
        });
      } else {
        document.querySelectorAll('.channel-admin').forEach((elm) => {
          elm.classList.add('hidden');
        });
      }
    }).catch((e) => {
      console.debug(e);
    });
  } else {
    getChannel(channelid).then(updateChannel);
    configready.then(() => {
      setSubscribed(subscribebtn, false);
    });
  }
}

function updateChannel(channel) {
  console.debug("Channel", channel);
  setSubscribed(document.querySelector('.toggle-subscription'), channel.subscribed);
  setText('[data-replace=channel-name]', channel.name);
  setText('[data-replace=domain]', window.location.hostname);
  setText('[data-replace=channel-id]', channel.id);
  setText('[data-replace=channel-subscriber-count]', channel.subscribers || 0);
  setText('[data-replace=channel-subscriber-count-plural]', channel.subscribers == 1 ? '' : 's');
  if(channel.token) {
    console.log("Channel token:", channel.token);
    document.querySelectorAll('.admin-btn').forEach((elm) => {
      elm.dataset.channel = channel.id;
      elm.dataset.token = channel.token;
    });
    document.querySelectorAll('[data-replace=token]').forEach((elm) => {
      elm.innerText = channel.token;
    });
  }
  return channel;
}

function renderHomepage() {
  if(localStorage.length > 0) {
    document.querySelector('.channel-list-header').classList.remove('hidden');
  }
  var ul = document.querySelector('.channel-list');
  ul.innerText = "";
  for(var channelid in localStorage) {
    if(localStorage.hasOwnProperty(channelid)) {
      var channel = getChannel(channelid, undefined, true);
      console.log("Adding channel to the list", channel);
      var a = document.createElement('a');
     a.innerText = channel.name;
      a.href = "#" + channel.id;
      var li = document.createElement('li');
      li.appendChild(a);
      ul.appendChild(li);
      console.log(li);
    }
  }

  document.querySelectorAll('.no-channel-box').forEach((elm) => {elm.classList.remove('hidden');});
}

function createChannel(e) {
  e.preventDefault();
  var createbtn = this.querySelector('button');
  createbtn.disabled = true;
  var originalText = createbtn.innerText;
  createbtn.innerText = "Creating...";
  var input = this.querySelector('.channel-name-input');
  var body = {"name": input.value};
  f('/api/v1/channel', 'POST', false, body).then(saveChannel).then((c) => {
    window.location.hash = c.id;
    input.disabled = false;
    createbtn.innerText = originalText;
    createbtn.disabled = false;
    input.value = "";
  });
}

function saveChannel(channel) {
  var old = {};
  if(channel.id in localStorage) {
    try {
      old = JSON.parse(localStorage.getItem(channel.id));
    } catch(e){}
  }
  channel = mergeObjs(channel, old);
  localStorage.setItem(channel.id, JSON.stringify(channel));
  return channel;
}

function toggleSubscription(e) {
  var path = '/api/v1/channel/' + this.dataset.channel + '/' + this.dataset.action || 'subscribe';
  var pushSubscription = requestNotificationAccess();
  pushSubscription
  .then((s) => {
    return f(path, 'PUT', s);
  })
  .then((c) => {
    setSubscribed(this, c.subscribed);
  })
  .catch((e) => {console.log(e);});
}


function onHashChange() {
  var channel = window.location.hash === "" ? false : window.location.hash.substring(1);
  ws.send({action: "unsubscribeAll"});
  console.log('Detected channel as', channel);
  if(channel) {
    document.querySelectorAll('.channel-box').forEach((elm) => {elm.classList.remove('hidden');});
    document.querySelectorAll('.no-channel-box').forEach((elm) => {elm.classList.add('hidden');});
  } else {
    document.querySelectorAll('.channel-box').forEach((elm) => {elm.classList.add('hidden');});
    document.querySelectorAll('.no-channel-box').forEach((elm) => {elm.classList.remove('hidden');});
    document.querySelectorAll('.channel-admin').forEach((elm) => {elm.classList.add('hidden');});
  }
  if(channel) {
    console.debug("Rendering channel", channel);
    renderChannel(channel);
  } else {
    renderHomepage();
  }
}

function detectNotificationsApproved() {
  return Notification.permission == "granted";
}

function getChannel(id, subscription, nonetwork) {
  var old = {}
  if(localStorage.hasOwnProperty(id)) {
    old = JSON.parse(localStorage.getItem(id));
  }
  if(nonetwork) {
    return old;
  }
  var status = subscription ? "/status" : "";
  return f('/api/v1/channel/' + id + status, 'GET', subscription)
  .then((channel) => {
    return mergeObjs(channel, old);
  });
}

function setSubscribed(button, subscribed) {
  if(subscribed) {
    button.classList.add('btn-blue');
    button.classList.remove('btn-green');
  } else {
    button.classList.remove('btn-blue');
    button.classList.add('btn-green');
  }
  button.disabled = false;
  button.dataset.action = subscribed ? 'unsubscribe' : 'subscribe';
  button.innerText = subscribed ? 'Unsubscribe' : 'Subscribe';
}

function requestNotificationAccess() {
  return navigator.serviceWorker.register('/service-worker.js')
  .then((r) => {return r.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: urlB64ToUint8Array(config.applicationServerKey)
  });});
}

function deleteChannel() {
  console.debug("Deleting Channel");
  this.disabled = true;
  f('/api/v1/channel/' + this.dataset.channel + '/delete', 'DELETE', undefined, {token: this.dataset.token}).then(() => {
    localStorage.removeItem(this.dataset.channel);
    window.location.hash = "#";
    this.disabled = false;
  });
}

function testChannel() {
  var postBody = {
    title: "Piston Test",
    token: this.dataset.token,
    options: {
      body: "This is a test of Piston"
    },
    browserURL: window.location.href
  }
  this.disabled = true;
  f('/api/v1/channel/' + this.dataset.channel + '/publish', 'POST', undefined, postBody).then(() => {
    this.disabled = false;
  });
}

function renderQRCode() {
  console.log(QRCode);
  QRCode.toCanvas(document.querySelector('.channel-qr-code'), window.location.href).then(() => {
    document.querySelector('.channel-qr-code').classList.remove('hidden');
  });
}

/* A wrapper for fetch that performs a number of common operations */
function f(url, method, auth, body) {
  console.debug(method, url, body);
  var headers = {};

  if(auth) {
    headers["X-Webpush-Subscription"] = JSON.stringify(auth.toJSON());
  }

  if(body) {
    body = JSON.stringify(body);
    headers['Content-Type'] = "application/json";
  }

  return fetch(url, {
    method: method || "GET",
    body: body,
    headers: headers
  }).then((r) => {if(r.status != 204) {return r.json()}});
}

/* This function appears to be required for chrome, i dunno why they can't just do it for me */
function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

// Merge two dictionaries, perferring the value in a
function mergeObjs(a, b) {
  var out = a;
  // This apparently is how to get the union of two arrays in JS? Returns a list of all keys that appear in at least one of these objects
  var keys = new Set([...Object.keys(a), ...Object.keys(b)])
  keys.forEach((key) => {
    if(a[key] === undefined) {
      out[key] = b[key];
    }
  });
  return out;
}

init()
