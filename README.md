# Piston

*A tool for facilitiating HTML5 push notifications*

## Archiecture

Piston consists of two components: A front-end webserver that accepts inbound notifications and handles things like
authn/authz. These are passed to a redis server, which the worker component reads from and sends the actual requests out,
handling things like re-delivery and reporting failures.
