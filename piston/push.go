package piston

import (
	"fmt"

	webpush "github.com/sherclockholmes/webpush-go"
)

type WebpushKeys struct {
	P256dh string
	Auth   string
}

type WebpushSubscription struct {
	Endpoint string
	Keys     WebpushKeys
}

func GenerateVapid() {
	fmt.Println("Checking for vapid keys..")
	keys := ConfigGet("vapid-public-key")
	fmt.Println("Got VAPID key. Public key: ", keys)
	if keys == "" {
		fmt.Println("No VAPID keys! Generating them now...")
		privateKey, publicKey, err := webpush.GenerateVAPIDKeys()
		Crash(err)
		ConfigSet("vapid-private-key", privateKey)
		ConfigSet("vapid-public-key", publicKey)
	}
}
