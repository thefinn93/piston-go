package piston

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

func AddHeaders(next http.Handler) http.Handler {
	reportUri := ConfigGet("report-uri")
	domain := ConfigGet("domain")
	cspValue := fmt.Sprintf("default-src 'self'; worker-src 'self'; connect-src 'self' wss://%v;", domain)
	if reportUri != "" {
		cspValue = fmt.Sprintf("%s report-uri %s", cspValue, reportUri)
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Security-Policy", cspValue)
		next.ServeHTTP(w, r)
	})
}

func FileServerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%v %v %v", r.RemoteAddr, r.Method, r.URL.Path)
		if strings.HasPrefix(r.URL.Path, "/api") {
			next.ServeHTTP(w, r)
		} else {
			http.FileServer(http.Dir("./static")).ServeHTTP(w, r)
		}
	})
}
