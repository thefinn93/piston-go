package piston

func Crash(err error) {
	if err != nil {
		panic(err)
	}
}
