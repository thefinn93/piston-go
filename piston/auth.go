package piston

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"github.com/kjk/betterguid"

	webpush "github.com/sherclockholmes/webpush-go"
)

func WebpushSubscriptionAuth(token string) (*BrowserSubscription, error) {
	auth := webpush.Subscription{}
	Crash(json.Unmarshal([]byte(token), &auth))
	authQuery := `SELECT * FROM browser_subscriptions WHERE endpoint = $1 AND p256dh = $2 AND auth = $3`
	subscription := &BrowserSubscription{}
	err := DB.QueryRowx(authQuery, auth.Endpoint, auth.Keys.P256dh, auth.Keys.Auth).StructScan(subscription)
	if err == sql.ErrNoRows {
		id := betterguid.New()
		insertQuery := "INSERT INTO browser_subscriptions (id, endpoint, p256dh, auth) VALUES ($1, $2, $3, $4)"
		DB.Exec(insertQuery, id, auth.Endpoint, auth.Keys.P256dh, auth.Keys.Auth)
		return WebpushSubscriptionAuth(token)
	} else {
		Crash(err)
	}
	fmt.Println("Authn:", subscription)
	return subscription, nil
}
