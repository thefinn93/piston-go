package piston

import "database/sql"

type ConfigItem struct {
	Key   string
	Value string
}

func ConfigSet(key string, value string) {
	DB.MustExec("INSERT INTO config (key, value) VALUES ($1, $2) ON CONFLICT (key) DO UPDATE SET value = $3", key, value, value)
}

func ConfigGet(key string) string {
	row := ConfigItem{}
	err := DB.Get(&row, "SELECT * FROM config WHERE key=$1", key)
	if err != sql.ErrNoRows {
		Crash(err)
	}
	return row.Value
}
