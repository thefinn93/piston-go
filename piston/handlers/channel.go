package handlers

import (
	"database/sql"
	"github.com/go-openapi/runtime/middleware"

	"github.com/kjk/betterguid"

	models "git.callpipe.com/finn/piston-go/models"
	piston "git.callpipe.com/finn/piston-go/piston"
	operations "git.callpipe.com/finn/piston-go/restapi/operations"
)

func GetChannel(params operations.GetChannelChannelParams) middleware.Responder {
	response := channelStatus(params.Channel)
	err := piston.DB.QueryRowx("SELECT id, name, link FROM channels WHERE id = $1", params.Channel).StructScan(response)
	piston.Crash(err)
	return operations.NewGetChannelChannelOK().WithPayload(response)
}

func GetChannelStatus(params operations.GetChannelChannelStatusParams, subscription *piston.BrowserSubscription) middleware.Responder {
	payload := channelStatus(params.Channel)
	authenticatedChannelStatus(payload, subscription)
	return operations.NewGetChannelChannelStatusOK().WithPayload(payload)
}

func channelStatus(channel string) *models.Channel {
	response := models.Channel{}
	err := piston.DB.QueryRowx("SELECT id, name, link FROM channels WHERE id = $1", channel).StructScan(&response)
	piston.Crash(err)
	err = piston.DB.QueryRowx("SELECT COUNT(subscription) AS subscribers FROM channel_subscriptions WHERE channel = $1", channel).StructScan(&response)
	piston.Crash(err)
	return &response
}

func authenticatedChannelStatus(channel *models.Channel, subscription *piston.BrowserSubscription) {
	if subscription != nil {
		err := piston.DB.QueryRowx("SELECT * FROM channel_subscriptions WHERE channel = $1 AND subscription = $2", channel.ID, subscription.Id).StructScan(&piston.ChannelSubscription{})
		if err == sql.ErrNoRows {
			channel.Subscribed = false
		} else {
			piston.Crash(err) // This will panic if err != nil, so anything after this will only be executed if there is no err. This code is not clear on that.
			channel.Subscribed = true
		}
	}
}

func Subscribe(params operations.PutChannelChannelSubscribeParams, subscription *piston.BrowserSubscription) middleware.Responder {
	_, err := piston.DB.Exec("INSERT INTO channel_subscriptions (channel, subscription) VALUES ($1, $2);", params.Channel, subscription.Id)
	piston.Crash(err)
	channel := channelStatus(params.Channel)
	piston.PublishToWSChannel(channel.ID, &piston.WebsocketChannelMessage{Channel: channel})
	authenticatedChannelStatus(channel, subscription)
	return operations.NewPutChannelChannelSubscribeOK().WithPayload(channel)
}

func Unsubscribe(params operations.PutChannelChannelUnsubscribeParams, subscription *piston.BrowserSubscription) middleware.Responder {
	_, err := piston.DB.Exec("DELETE FROM channel_subscriptions WHERE channel = $1 AND subscription = $2;", params.Channel, subscription.Id)
	piston.Crash(err)
	channel := channelStatus(params.Channel)
	piston.PublishToWSChannel(channel.ID, &piston.WebsocketChannelMessage{Channel: channel})
	return operations.NewPutChannelChannelUnsubscribeOK().WithPayload(channel)
}

func CreateChannel(params operations.PostChannelParams) middleware.Responder {
	id := betterguid.New()
	token, err := piston.GenerateToken(30)
	piston.Crash(err)
	_, err = piston.DB.Exec("INSERT INTO channels (id, token, name, link) VALUES($1, $2, $3, $4)", id, token, params.Body.Name, params.Body.Link)
	piston.Crash(err)
	payload := models.Channel{
		ID:    id,
		Token: token,
		Name:  params.Body.Name,
		Link:  params.Body.Link,
	}
	return operations.NewPostChannelCreated().WithPayload(&payload)
}

func DeleteChannel(params operations.DeleteChannelChannelDeleteParams) middleware.Responder {
	_, err := piston.DB.Exec("DELETE FROM channel_subscriptions WHERE channel = $1", params.Channel)
	piston.Crash(err)
	_, err = piston.DB.Exec("DELETE FROM channel WHERE id = $1", params.Channel)
	piston.Crash(err)
	return operations.NewDeleteChannelChannelDeleteNoContent()
}
