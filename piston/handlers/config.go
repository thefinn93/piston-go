package handlers

import (
	"github.com/go-openapi/runtime/middleware"

	piston "git.callpipe.com/finn/piston-go/piston"
	operations "git.callpipe.com/finn/piston-go/restapi/operations"
)

func GetConfigHandler(params operations.GetConfigParams) middleware.Responder {
	domain := piston.ConfigGet("domain")

	payload := &operations.GetConfigOKBody{
		ApplicationServerKey: piston.ConfigGet("vapid-public-key"),
		Domain:               domain,
	}
	return operations.NewGetConfigOK().WithPayload(payload)
}
