package handlers

import (
	"encoding/json"

	"github.com/go-openapi/runtime/middleware"
	webpush "github.com/sherclockholmes/webpush-go"

	models "git.callpipe.com/finn/piston-go/models"
	piston "git.callpipe.com/finn/piston-go/piston"
	operations "git.callpipe.com/finn/piston-go/restapi/operations"
)

type Payload struct {
	title string
	body  string
}

func Publish(params operations.PostChannelChannelPublishParams) middleware.Responder {
	browserSubscriptions := []piston.BrowserSubscription{}
	err := piston.DB.Select(&browserSubscriptions, `SELECT browser_subscriptions.* FROM browser_subscriptions
		  LEFT JOIN channel_subscriptions ON browser_subscriptions.id = channel_subscriptions.subscription
		  LEFT JOIN channels ON channels.id = channel_subscriptions.channel
		WHERE channels.token = $1`, params.Body.Token)
	piston.Crash(err)

	channel := models.Channel{}
	err = piston.DB.QueryRowx(`SELECT * FROM channels WHERE token = $1`, params.Body.Token).StructScan(&channel)

	piston.Crash(err)

	params.Body.Token = ""
	payload, err := json.Marshal(params.Body)
	piston.Crash(err)

	for _, browserSubscription := range browserSubscriptions {
		s := browserSubscription.Subscription()
		_, err = webpush.SendNotification([]byte(payload), &s, &webpush.Options{
			Subscriber:      "ops@piston.ninja",
			VAPIDPrivateKey: piston.ConfigGet("vapid-private-key"),
		})
		piston.Crash(err)
	}

	wsmsg := piston.WebsocketChannelMessage{Notification: params.Body}
	piston.PublishToWSChannel(channel.ID, &wsmsg)

	return operations.NewPostChannelChannelPublishNoContent()
}
