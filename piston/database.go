package piston

import (
	"fmt"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	webpush "github.com/sherclockholmes/webpush-go"
)

var DB *sqlx.DB

type BrowserSubscription struct {
	Id       string
	Endpoint string
	P256dh   string
	Auth     string
}

type Channel struct {
	Id    string
	Token string
	Name  string
	Link  string
}

type ChannelSubscription struct {
	Channel      string
	Subscription string
}

func BuildBrowserSubscription(subscription webpush.Subscription) BrowserSubscription {
	browserSubscription := BrowserSubscription{
		Endpoint: subscription.Endpoint,
		P256dh:   subscription.Keys.P256dh,
		Auth:     subscription.Keys.Auth,
	}
	return browserSubscription
}

func (browserSubscription *BrowserSubscription) Subscription() webpush.Subscription {
	subscription := webpush.Subscription{
		Endpoint: browserSubscription.Endpoint,
		Keys: webpush.Keys{
			Auth:   browserSubscription.Auth,
			P256dh: browserSubscription.P256dh,
		},
	}
	return subscription
}

func InitDB() {
	fmt.Println("Connecting to database...")
	DB = sqlx.MustConnect("postgres", os.Getenv("DB_CONNECTION_STRING"))
	DB.MustExec(`CREATE TABLE IF NOT EXISTS browser_subscriptions (id TEXT PRIMARY KEY, endpoint TEXT, p256dh TEXT, auth TEXT);`)
	DB.MustExec(`CREATE TABLE IF NOT EXISTS channels (id TEXT PRIMARY KEY, token TEXT, name TEXT, link TEXT);`)
	DB.MustExec(`CREATE TABLE IF NOT EXISTS channel_subscriptions (channel TEXT REFERENCES channels(id), subscription TEXT REFERENCES browser_subscriptions(id), unique(channel, subscription))`)
	DB.MustExec(`CREATE TABLE IF NOT EXISTS config (key VARCHAR(20) PRIMARY KEY, value TEXT);`)
	fmt.Println("Created config")
}
