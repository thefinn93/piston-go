package piston

import (
	"encoding/json"
	"log"

	"git.callpipe.com/finn/piston-go/models"
	"github.com/gorilla/websocket"
)

type WebsocketChannelMessage struct {
	Channel      *models.Channel
	Notification *models.Notification
}

// These two variables both store a list of clients connected to channels
// The channels is indexed by channel UUID, clients is indexed by client connection pointer
var channels = make(map[string]map[*websocket.Conn]bool)
var clients = make(map[*websocket.Conn]map[string]bool)

func SubscribeWS(channelid string, conn *websocket.Conn) {
	if _, ok := channels[channelid]; !ok {
		channels[channelid] = make(map[*websocket.Conn]bool)
		log.Printf("socket list for channel %v did not exist, created now", channelid)
	}
	channels[channelid][conn] = true

	if _, ok := clients[conn]; !ok {
		clients[conn] = make(map[string]bool)
		log.Printf("Created new subscription list for this client!")
	}
	clients[conn][channelid] = true

	log.Printf("New subscription to %v (all chanels: %v)", channelid, channels)
}

func UnsubscribeWS(channelid string, conn *websocket.Conn) {
	if _, ok := channels[channelid]; ok {
		delete(channels[channelid], conn)
	}
	if _, ok := clients[conn]; ok {
		delete(clients[conn], channelid)
	}
}

// CloseWS is called when the connection dies
func UnsubscribeAllWS(conn *websocket.Conn) {
	if clientChannels, ok := clients[conn]; ok {
		for channel := range clientChannels {
			log.Printf("Unsubscribing from channel %v", channel)
			delete(channels[channel], conn)
		}
		delete(clients, conn)
	}
}

func PublishToWSChannel(channel string, message *WebsocketChannelMessage) {
	jsonmessage, err := json.Marshal(message)
	Crash(err)
	log.Printf("Publishing to %v subscriber(s): %v", len(channels[channel]), string(jsonmessage))
	for client := range channels[channel] {
		client.WriteMessage(websocket.TextMessage, jsonmessage)
	}
}
