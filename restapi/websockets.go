package restapi

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"

	"git.callpipe.com/finn/piston-go/models"
	"git.callpipe.com/finn/piston-go/piston"
	"git.callpipe.com/finn/piston-go/piston/handlers"
	"git.callpipe.com/finn/piston-go/restapi/operations"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(_ *http.Request) bool { return true },
}

type WebsocketRequest struct {
	Action     string
	Parameters map[string]interface{}
}

type CreateChannelRequest struct {
	Action     string
	Parameters operations.PostChannelBody
}

type SubscribeRequest struct {
	Action  string
	Channel string
}

type WebsocketChannelMessage struct {
	Channel      *models.Channel
	Notification *models.Notification
}

func BindWebsocket(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/ws" {
			handleWebsocket(w, r)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func handleWebsocket(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	piston.Crash(err)
	defer conn.Close()

	for {
		// First value is the message type. All message types are in gorilla websocket's conn.go
		_, rawrequest, err := conn.ReadMessage()
		if err != nil {
			log.Printf("Websocket error: %v", err)
			piston.UnsubscribeAllWS(conn)
			break
		}

		// Unmarshal the JSON blob sent
		requestaction := WebsocketRequest{}
		piston.Crash(json.Unmarshal(rawrequest, &requestaction))

		// Print the raw message to the stdout, for debugging (eventually something more like an access log should be done)
		fmt.Println(requestaction)

		switch action := requestaction.Action; action {
		case "createChannel":
			request := CreateChannelRequest{}
			piston.Crash(json.Unmarshal(rawrequest, &request))
			fmt.Println("Request:", request)
			params := operations.PostChannelParams{Body: request.Parameters}
			response := handlers.CreateChannel(params).(*operations.PostChannelCreated)
			jsonresponse, err := json.Marshal(response.Payload)
			piston.Crash(err)
			fmt.Println("Response:", string(jsonresponse))
			piston.Crash(conn.WriteMessage(websocket.TextMessage, jsonresponse))
		case "subscribe":
			request := SubscribeRequest{}
			piston.Crash(json.Unmarshal(rawrequest, &request))
			piston.SubscribeWS(request.Channel, conn)
		case "unsubscribe":
			request := SubscribeRequest{}
			piston.Crash(json.Unmarshal(rawrequest, &request))
			piston.UnsubscribeWS(request.Channel, conn)
		case "unsubscribeAll":
			piston.UnsubscribeAllWS(conn)
		}
	}
}
