package restapi

import (
	"crypto/tls"
	"net/http"
	"fmt"

	errors "github.com/go-openapi/errors"
	runtime "github.com/go-openapi/runtime"

	operations "git.callpipe.com/finn/piston-go/restapi/operations"
	handlers "git.callpipe.com/finn/piston-go/piston/handlers"
	piston "git.callpipe.com/finn/piston-go/piston"
)

func configureFlags(api *operations.PistonAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.PistonAPI) http.Handler {
	piston.InitDB() // This *should* be in configureServer but that runs after this, and some of these need the DB

	api.ServeError = errors.ServeError

//	api.Logger = log.Printf

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.WebpushSubscriptionAuth = piston.WebpushSubscriptionAuth

	api.GetConfigHandler = operations.GetConfigHandlerFunc(handlers.GetConfigHandler)

	api.PostChannelHandler = operations.PostChannelHandlerFunc(handlers.CreateChannel)

	api.GetChannelChannelHandler = operations.GetChannelChannelHandlerFunc(handlers.GetChannel)

	api.GetChannelChannelStatusHandler = operations.GetChannelChannelStatusHandlerFunc(handlers.GetChannelStatus)

	api.PostChannelChannelPublishHandler = operations.PostChannelChannelPublishHandlerFunc(handlers.Publish)

	api.PutChannelChannelSubscribeHandler = operations.PutChannelChannelSubscribeHandlerFunc(handlers.Subscribe)

	api.PutChannelChannelUnsubscribeHandler = operations.PutChannelChannelUnsubscribeHandlerFunc(handlers.Unsubscribe)

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
	//piston.InitDB() // This should be called here, but is called at the top of configureAPI so API handlers can use the DB
	piston.GenerateVapid()
	fmt.Println("Done with initial setup")
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	handler = piston.FileServerMiddleware(handler)
	handler = piston.AddHeaders(handler)
	handler = BindWebsocket(handler)
	return handler
}
